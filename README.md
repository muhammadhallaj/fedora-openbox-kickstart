# Fedora Openbox Kickstart

## Usage

### Building natively on Fedora

1. Ensure both `make` and `mock` is installed on the machine.
1. Run `make` to initialize the mock environment and have the ISO file
   generated.

### Building through docker

1. Ensure `docker` is installed on the machine.
1. Run `make docker` to have the `Fedora` image installed and build triggered.
1. The generated ISO would be copied over to the same directory.

## Configuration option

The `Makefile` will generate `fedora-32-x86_64` image. This could be
overwritten by using `FOKS_VERSION`.

Setting `FOKS_VERSION=fedora-31-x86_64` before `make` would build
`fedora-31-x86_64`.
