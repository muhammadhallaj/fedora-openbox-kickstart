#!/usr/bin/make -f

FOKS_VERSION := fedora-34-x86_64
KS_LIVE_CONF := fedora-live-openbox.ks
KS_LIVE_NAME := Fedora-Openbox-Live
DOCKER_IMAGE := inu-io/kickstart
DOCKER_CNAME := fedora_live_openbox

all: archive

clean:
	@echo "Cleaning up the mock environment"
	mock -r $(FOKS_VERSION) --clean

init: clean
	@echo "Initializing the mock environment"
	mock -r $(FOKS_VERSION) --init

dependencies: init
	@echo "Installing dependencies in the mock environment"
	mock -r $(FOKS_VERSION) --install lorax-lmc-novirt

flatten: dependencies
	@echo "Flattening the fedora kickstart file"
	ksflatten \
	  --config $(KS_LIVE_CONF) \
	  -o /var/lib/mock/$(FOKS_VERSION)/root/tmp/$(KS_LIVE_CONF)
	sed -i '/^repo --name="rawhide"/d' \
	  /var/lib/mock/$(FOKS_VERSION)/root/tmp/$(KS_LIVE_CONF)

iso: flatten
	@echo "Building the ISO"
	mock -r $(FOKS_VERSION) \
	  --enable-network \
	  --chroot -- \
		"livemedia-creator \
			--no-virt \
			--make-iso \
			--nomacboot \
			--iso-only \
			--releasever=34 \
			--iso-name=$(KS_LIVE_NAME)-x86_64.iso \
			--project=$(KS_LIVE_NAME) \
			--volid=$(KS_LIVE_NAME) \
			--ks=/tmp/$(KS_LIVE_CONF) \
			--resultdir=/tmp/lmc"

archive: iso
	cp /var/lib/mock/$(FOKS_VERSION)/root/tmp/lmc/*.iso .

docker.clean:
	@echo "Removing running containers"
	docker rm -f $(DOCKER_CNAME) || true

docker.build:
	@echo "Building the docker image"
	docker build -t $(DOCKER_IMAGE) dockerfiles

docker.create:
	@echo "Starting the docker container"
	docker run \
	  --name $(DOCKER_CNAME) \
	  -it \
	  --privileged \
	  -v `pwd`:/apps \
	  -v `pwd`/.cache:/var/cache \
	  -w /apps \
	  $(DOCKER_IMAGE) make iso

docker.archive:
	docker cp $(DOCKER_CNAME):/var/lib/mock/$(FOKS_VERSION)/root/tmp/lmc/$(KS_LIVE_NAME)-x86_64.iso $(KS_LIVE_NAME)-x86_64.iso

docker: docker.clean docker.build docker.create docker.archive
