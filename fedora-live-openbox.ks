# fedora-live-openbox.ks

## default kickstart images we'll inherit
%include /usr/share/spin-kickstarts/fedora-live-base.ks
%include /usr/share/spin-kickstarts/fedora-live-minimization.ks
%include /usr/share/spin-kickstarts/fedora-repo-not-rawhide.ks

## default configuration
timezone UTC
part / --size 6656

## repositories we're enabling
%include fedora-repo-common.ks

## packages kickstart file we'll inherit
%include fedora-openbox-common.ks

%post --nochroot
## adding resolver to ensure we could lookup URLs to download from
cp /etc/resolv.conf /mnt/sysimage/etc/resolv.conf
%end

%post
## setting up needed skels directory

## flatpak setup
### setting up flathub repo
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
### setup spotify
#flatpak install --assumeyes --noninteractive https://flathub.org/repo/appstream/com.spotify.Client.flatpakref

## fonts setup
### getting Inconsolata for Powerline
mkdir -p /usr/share/fonts/Inconsolata
wget -P /usr/share/fonts/Inconsolata/ https://github.com/powerline/fonts/raw/master/Inconsolata/Inconsolata%20for%20Powerline.otf
wget -P /usr/share/fonts/Inconsolata/ https://github.com/powerline/fonts/raw/master/Inconsolata/Inconsolata%20Bold%20for%20Powerline.ttf
fc-cache -fv

## gtk setup
mkdir -p /etc/skel/.config/gtk-3.0
wget -O /etc/skel/.gtkrc-2.0 https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/gtk-2.0/gtkrc
wget -O /etc/skel/.config/gtk-3.0/settings.ini https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/gtk-3.0/settings.ini

## openbox setup
### configure openbox to use Minstral-Thin
wget -O /etc/xdg/openbox/rc.xml https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/openbox/rc.xml

## configure tint2
wget -O /etc/xdg/autostart/tint2.desktop https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/xdg/autostart/tint2.desktop
wget -O /etc/xdg/tint2/tint2rc https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/tint2/tint2rc

## configure pcmanfm
mkdir -p /etc/skel/.config/pcmanfm/default
wget -O /etc/skel/.config/pcmanfm/default/desktop-items-0.conf https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/pcmanfm/desktop-items-0.conf
wget -O /etc/xdg/autostart/pcmanfm.desktop https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/xdg/autostart/pcmanfm.desktop

## configure flameshot
wget -O /etc/xdg/autostart/flameshot.desktop https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/xdg/autostart/flameshot.desktop

## lightdm tweak, to ensure our Xresources loads fine
#sed -i 's: -nocpp::g' /etc/X11/xinit/xinitrc-common

## Xresources setup
mkdir -p /etc/skel/.Xresources.d
### fetch the Xresource stuffs from git repositories
wget -O /etc/skel/.Xresources https://gitlab.com/muhammadhallaj/dotfiles/raw/master/Xresources
wget -P /etc/skel/.Xresources.d/ https://gitlab.com/muhammadhallaj/dotfiles/raw/master/Xresources.d/urxvt.Xresources
wget -P /etc/skel/.Xresources.d/ https://raw.githubusercontent.com/base16-templates/base16-xresources/master/xresources/base16-zenburn-256.Xresources

## setting a default tmux.conf
wget -O /etc/skel/.tmux.conf https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/tmux.conf

## setting a default volumeicon
mkdir -p /etc/skel/.config/volumeicon
wget -O /etc/skel/.config/volumeicon/volumeicon https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/volumeicon/volumeicon

## setting up brave browser
dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
#dnf install -y brave-browser

## setting up zshrc
ZSH=/etc/skel/.oh-my-zsh sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

mkdir -p /etc/skel/.zshrc.d

wget -O /etc/skel/.zshrc https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/zshrc
wget -O /etc/skel/.zshrc.d/oh-my-zsh.sh https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/zshrc.d/oh-my-zsh.sh
wget -O /etc/skel/.zshrc.d/ssh-agent.sh https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/zshrc.d/ssh-agent.sh
wget -O /etc/skel/.zshrc.d/tmux.sh https://gitlab.com/muhammadhallaj/dotfiles/-/raw/master/zshrc.d/tmux.sh

sed -i 's:SHELL=/bin/bash:SHELL=/bin/zsh:g' /etc/default/useradd

## cleaning up rpm packages
dnf -y autoremove
dnf -y clean all

## live-cd stuffs
cat > /etc/sysconfig/desktop << eof
PREFERRED=/usr/bin/openbox-session
DISPLAYMANAGER=/usr/sbin/lightdm
eof

cat >> /etc/rc.d/init.d/livesys << eof

# set up lightdm autologin
sed -i 's/^#autologin-user=.*/autologin-user=liveuser/' /etc/lightdm/lightdm.conf
sed -i 's/^#autologin-user-timeout=.*/autologin-user-timeout=0/' /etc/lightdm/lightdm.conf
sed -i 's/^#user-session=.*/user-session=openbox/' /etc/lightdm/lightdm.conf

# Show harddisk install on the desktop
sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop
mkdir /home/liveuser/Desktop
cp /usr/share/applications/liveinst.desktop /home/liveuser/Desktop

# and mark it as executable
chmod +x /home/liveuser/Desktop/liveinst.desktop

# this goes at the end after all other changes.
chown -R liveuser:liveuser /home/liveuser
restorecon -R /home/liveuser
eof
%end
